<?php

namespace tests\codeception\unit\models;

use yii\codeception\TestCase;
use app\models\User;
class ExampleTest extends TestCase
{

	
    // TODO add test methods here
	
	 public function testCreateUser() {
         $m = new User();
         $m->name = "User";
         $m->email = "myser@email.com";
         $this->assertTrue($m->save());
      }
      public function testUpdateUser() {
         $m = new User();
         $m->name = "User";
         $m->email = "myser2@email.com";
         $this->assertTrue($m->save());
         $this->assertEquals("User2", $m->name);
      }
      public function testDeleteUser() {
         $m = User::findOne(['name' => 'User']);
         $this->assertNotNull($m);
         User::deleteAll(['name' => $m->name]);
         $m = User::findOne(['name' => 'User']);
         $this->assertNull($m);
      }
}
